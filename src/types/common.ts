import { Effect, Event, Store } from 'effector';
import { ZodSchema } from 'zod';

export type FormValidationParams<V> = {
  values: V;
  schema: ZodSchema<Partial<V>>;
};

export type FormPathValidationParams<V> = {
  schema: ZodSchema<Partial<V>>;
  path: FormPath;
  value: any;
};

export type FormPath = string;

export type FormErrors = Record<FormPath, string>;

/** Конфигурация формы, аргументы функции createForm */
export type FormConfig<V> = Partial<{
  initialValues: V | Store<V>;
  validationSchema: ZodSchema<Partial<V>> | null;
  onSubmit: any;
  onReject: any;
  resetOn: any;
  initCurrentOn: any;
  initValuesOn: any;
  submitOnChange: boolean;
  editable: boolean;
  debug: boolean;
  sid: string;
}>;

/** Внешние и внутренние юниты формы */
export type FormUnits<V> = {
  $values: Store<V>;
  $initialValues: Store<V>;
  $editable: Store<boolean>;
  $valid: Store<boolean>;
  $dirty: Store<boolean>;
  $submittable: Store<boolean>;
  $validationSchema: Store<ZodSchema<Partial<V>> | null>;
  $errors: Store<FormErrors>;

  submit: Event<void>;
  reset: Event<void>;
  initCurrent: Event<void>;
  initValues: Event<V>;
  setEditable: Event<boolean>;
  setValue: Event<{ path: FormPath; value: any }>;
  setValues: Event<V>;
  updateValues: Event<Partial<V>>;
  arrayUnshift: Event<{ path: FormPath; value: any }>;
  arrayPush: Event<{ path: FormPath; value: any }>;
  arrayDelete: Event<{ path: FormPath; index: number }>;
  arrayMove: Event<{ path: FormPath; source: number; target: number }>;
  setValidationSchema: Event<ZodSchema<Partial<V>> | null>;
  externalValidate: Event<void>;
  internalValidate: Event<void>;

  submitted: Event<V>;
  rejected: Event<FormErrors>;

  fxExternalValidate: Effect<FormValidationParams<V>, void, FormErrors>;
  fxInternalValidate: Effect<FormValidationParams<V>, void, FormErrors>;
  fxPathValidate: Effect<FormPathValidationParams<V>, void, FormErrors>;
};

/** Основная сущность создаваемой формы */
export type FormInstance<V> = {
  $values: FormUnits<V>['$values'];
  $initialValues: FormUnits<V>['$initialValues'];
  $editable: FormUnits<V>['$editable'];
  $valid: FormUnits<V>['$valid'];
  $dirty: FormUnits<V>['$dirty'];
  $submittable: FormUnits<V>['$submittable'];
  $validationSchema: FormUnits<V>['$validationSchema'];
  $errors: FormUnits<V>['$errors'];

  values: FormUnits<V>['$values'];
  initialValues: FormUnits<V>['$initialValues'];
  editable: FormUnits<V>['$editable'];
  valid: FormUnits<V>['$valid'];
  dirty: FormUnits<V>['$dirty'];
  submittable: FormUnits<V>['$submittable'];
  validationSchema: FormUnits<V>['$validationSchema'];
  errors: FormUnits<V>['$errors'];

  submit: FormUnits<V>['submit'];
  reset: FormUnits<V>['reset'];
  initCurrent: FormUnits<V>['initCurrent'];
  initValues: FormUnits<V>['initValues'];
  setEditable: FormUnits<V>['setEditable'];
  setValue: FormUnits<V>['setValue'];
  setValues: FormUnits<V>['setValues'];
  updateValues: FormUnits<V>['updateValues'];
  arrayUnshift: FormUnits<V>['arrayUnshift'];
  arrayPush: FormUnits<V>['arrayPush'];
  arrayDelete: FormUnits<V>['arrayDelete'];
  arrayMove: FormUnits<V>['arrayMove'];
  setValidationSchema: FormUnits<V>['setValidationSchema'];
  validate: FormUnits<V>['externalValidate'];

  submitted: FormUnits<V>['submitted'];
  rejected: FormUnits<V>['rejected'];
};
