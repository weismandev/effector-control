import { ZodSchema } from 'zod';

export const validatePath = async ({
  schema,
  path,
  value,
}: {
  schema: ZodSchema<any>;
  path: string;
  value: any;
}) => {
  const validation = schema.safeParse(value);
  const errors: Record<string, string> = {};

  if (!validation.success) {
    validation.error.format((error) => {
      errors[path] = error.message;
    });

    throw errors;
  }
};
