import { describe, expect, test } from 'vitest';

import { isInteger, isObject, isString } from '@/src/lib';

describe('isInteger', () => {
  test('Базовый', () => {
    expect(isInteger('1')).toBe(true);
  });

  test('С неправильным типом', () => {
    // @ts-ignore
    expect(isInteger()).toBe(false);
    expect(isInteger(null)).toBe(false);
    expect(isInteger(123)).toBe(false);
    expect(isInteger('abc')).toBe(false);
    expect(isInteger({})).toBe(false);
    expect(isInteger({ a: 1 })).toBe(false);
    expect(isInteger(['abc'])).toBe(false);
  });
});

describe('isObject', () => {
  test('Базовый', () => {
    expect(isObject({ test: true })).toBe(true);
  });

  test('С неправильным типом', () => {
    // @ts-ignore
    expect(isObject()).toBe(false);
    expect(isObject(null)).toBe(false);
    expect(isObject(123)).toBe(false);
    expect(isObject('abc')).toBe(false);
    expect(isObject({})).toBe(true);
    expect(isObject({ a: 1 })).toBe(true);
    expect(isObject(['abc'])).toBe(true);
  });
});

describe('isString', () => {
  test('Базовый', () => {
    expect(isString('test')).toBe(true);
  });

  test('С неправильным типом', () => {
    // @ts-ignore
    expect(isString()).toBe(false);
    expect(isString(null)).toBe(false);
    expect(isString(123)).toBe(false);
    expect(isString('abc')).toBe(true);
    expect(isString({})).toBe(false);
    expect(isString({ a: 1 })).toBe(false);
    expect(isString(['abc'])).toBe(false);
  });
});
