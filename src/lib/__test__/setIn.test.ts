import { expect, test } from 'vitest';

import { setIn } from '@/src/lib';

test('sets flat value', () => {
  const obj = { x: 'y' };
  const newObj = setIn(obj, 'flat', 'value');
  expect(obj).toEqual({ x: 'y' });
  expect(newObj).toEqual({ x: 'y', flat: 'value' });
});

test('keep the same object if nothing is changed', () => {
  const obj = { x: 'y' };
  const newObj = setIn(obj, 'x', 'y');
  expect(obj).toBe(newObj);
});

test('removes flat value', () => {
  const obj = { x: 'y' };
  const newObj = setIn(obj, 'x', undefined);
  expect(obj).toEqual({ x: 'y' });
  expect(newObj).toEqual({});
  expect(newObj).not.toHaveProperty('x');
});

test('sets nested value', () => {
  const obj = { x: 'y' };
  const newObj = setIn(obj, 'nested.value', 'nested value');
  expect(obj).toEqual({ x: 'y' });
  expect(newObj).toEqual({ x: 'y', nested: { value: 'nested value' } });
});

test('updates nested value', () => {
  const obj = { x: 'y', nested: { value: 'a' } };
  const newObj = setIn(obj, 'nested.value', 'b');
  expect(obj).toEqual({ x: 'y', nested: { value: 'a' } });
  expect(newObj).toEqual({ x: 'y', nested: { value: 'b' } });
});

test('removes nested value', () => {
  const obj = { x: 'y', nested: { value: 'a' } };
  const newObj = setIn(obj, 'nested.value', undefined);
  expect(obj).toEqual({ x: 'y', nested: { value: 'a' } });
  expect(newObj).toEqual({ x: 'y', nested: {} });
  expect(newObj.nested).not.toHaveProperty('value');
});

test('updates deep nested value', () => {
  const obj = { x: 'y', twofoldly: { nested: { value: 'a' } } };
  const newObj = setIn(obj, 'twofoldly.nested.value', 'b');
  expect(obj.twofoldly.nested === newObj.twofoldly.nested).toEqual(false); // fails, same object still
  expect(obj).toEqual({ x: 'y', twofoldly: { nested: { value: 'a' } } }); // fails, test's b here, too
  expect(newObj).toEqual({ x: 'y', twofoldly: { nested: { value: 'b' } } }); // works ofc
});

test('removes deep nested value', () => {
  const obj = { x: 'y', twofoldly: { nested: { value: 'a' } } };
  const newObj = setIn(obj, 'twofoldly.nested.value', undefined);
  expect(obj.twofoldly.nested === newObj.twofoldly.nested).toEqual(false);
  expect(obj).toEqual({ x: 'y', twofoldly: { nested: { value: 'a' } } });
  expect(newObj).toEqual({ x: 'y', twofoldly: { nested: {} } });
  expect(newObj.twofoldly.nested).not.toHaveProperty('value');
});

test('shallow clone data along the update path', () => {
  const obj = {
    x: 'y',
    twofoldly: { nested: ['a', { c: 'd' }] },
    other: { nestedOther: 'o' },
  };
  const newObj = setIn(obj, 'twofoldly.nested.0', 'b');
  // All new objects/arrays created along the update path.
  expect(obj).not.toBe(newObj);
  expect(obj.twofoldly).not.toBe(newObj.twofoldly);
  expect(obj.twofoldly.nested).not.toBe(newObj.twofoldly.nested);
  // All other objects/arrays copied, not cloned (retain same memory
  // location).
  expect(obj.other).toBe(newObj.other);
  expect(obj.twofoldly.nested[1]).toBe(newObj.twofoldly.nested[1]);
});

test('sets new array', () => {
  const obj = { x: 'y' };
  const newObj = setIn(obj, 'nested.0', 'value');
  expect(obj).toEqual({ x: 'y' });
  expect(newObj).toEqual({ x: 'y', nested: ['value'] });
});

test('updates nested array value', () => {
  const obj = { x: 'y', nested: ['a'] };
  const newObj = setIn(obj, 'nested[0]', 'b');
  expect(obj).toEqual({ x: 'y', nested: ['a'] });
  expect(newObj).toEqual({ x: 'y', nested: ['b'] });
});

test('adds new item to nested array', () => {
  const obj = { x: 'y', nested: ['a'] };
  const newObj = setIn(obj, 'nested.1', 'b');
  expect(obj).toEqual({ x: 'y', nested: ['a'] });
  expect(newObj).toEqual({ x: 'y', nested: ['a', 'b'] });
});

test('sticks to object with int key when defined', () => {
  const obj = { x: 'y', nested: { 0: 'a' } };
  const newObj = setIn(obj, 'nested.0', 'b');
  expect(obj).toEqual({ x: 'y', nested: { 0: 'a' } });
  expect(newObj).toEqual({ x: 'y', nested: { 0: 'b' } });
});

test('supports bracket path', () => {
  const obj = { x: 'y' };
  const newObj = setIn(obj, 'nested[0]', 'value');
  expect(obj).toEqual({ x: 'y' });
  expect(newObj).toEqual({ x: 'y', nested: ['value'] });
});

test('supports path containing key of the object', () => {
  const obj = { x: 'y' };
  const newObj = setIn(obj, 'a.x.c', 'value');
  expect(obj).toEqual({ x: 'y' });
  expect(newObj).toEqual({ x: 'y', a: { x: { c: 'value' } } });
});

test('should keep class inheritance for the top level object', () => {
  class TestClass {
    constructor(public key: string, public setObj?: any) {}
  }
  const obj = new TestClass('value');
  const newObj = setIn(obj, 'setObj.nested', 'setInValue');
  expect(obj).toEqual(new TestClass('value'));
  expect(newObj).toEqual({
    key: 'value',
    setObj: { nested: 'setInValue' },
  });
  expect(obj instanceof TestClass).toEqual(true);
  expect(newObj instanceof TestClass).toEqual(true);
});

test('can convert primitives to objects before setting', () => {
  const obj = { x: [{ y: true }] };
  const newObj = setIn(obj, 'x.0.y.z', true);
  expect(obj).toEqual({ x: [{ y: true }] });
  expect(newObj).toEqual({ x: [{ y: { z: true } }] });
});
