import { ZodSchema } from 'zod';

export const validateSchema = async <V>({
  schema,
  values,
}: {
  schema: ZodSchema<Partial<V>>;
  values: V;
}) => {
  const validation = schema.safeParse(values);
  const errors: Record<string, string> = {};

  if (!validation.success) {
    validation.error.format((error) => {
      const flatPath = error.path.join('.');

      if (!errors[flatPath]) {
        errors[flatPath] = error.message;
      }
    });

    throw errors;
  }
};
