import { sample } from 'effector';
import isEqual from 'fast-deep-equal';

import { FormUnits } from '@/src';

export const stateModule = <V>(form: FormUnits<V>) => {
  const { setEditable, $dirty, $editable, $errors, $valid, $values, $initialValues } =
    form;

  // Установка режима редактирования
  sample({
    clock: setEditable,
    target: $editable,
  });

  // Установка валидности
  sample({
    source: $errors,
    fn: (errors) => Boolean(!Object.keys(errors).length),
    target: $valid,
  });

  // Установка чистоты
  sample({
    clock: $values,
    source: { values: $values, initialValues: $initialValues },
    fn: ({ values, initialValues }) => !isEqual(initialValues, values),
    target: $dirty,
  });
};
