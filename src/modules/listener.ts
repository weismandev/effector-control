import { is, sample } from 'effector';

import { FormConfig, FormUnits } from '@/src';

export const listenerModule = <V>(form: FormUnits<V>, config?: FormConfig<V>) => {
  const { submitted, rejected, reset, initCurrent, initValues, $values, submit } = form;
  const isExternalInitialValues = is.store(config?.initialValues);

  // Прокидываем успешный ввод формы во внешний юнит
  if (config?.onSubmit) {
    sample({
      clock: submitted,
      target: config.onSubmit,
    });
  }

  // Прокидываем неудачный ввод с ошибками во внешний юнит
  if (config?.onReject) {
    sample({
      clock: rejected,
      target: config.onReject,
    });
  }

  // Прокидываем сброс формы во внешний юнит
  if (config?.resetOn) {
    sample({
      // @ts-ignore
      clock: config.resetOn,
      target: reset,
    });
  }

  // Прокидываем текущую инициализацию формы во внешний юнит
  if (config?.initCurrentOn) {
    sample({
      // @ts-ignore
      clock: config.initCurrentOn,
      filter: () => !isExternalInitialValues,
      target: initCurrent,
    });
  }

  // Прокидываем внешнюю инициализацию формы во внешний юнит
  if (config?.initValuesOn) {
    sample({
      // @ts-ignore
      clock: config.initValuesOn,
      target: initValues,
    });
  }

  if (config?.submitOnChange) {
    sample({
      clock: $values,
      target: submit,
    });
  }
};
