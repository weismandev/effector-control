import { sample } from 'effector';

import { FormUnits } from '@/src';
import { getIn } from '@/src/lib';

export const arraysModule = <V>(form: FormUnits<V>) => {
  const {
    setValue,
    arrayUnshift,
    arrayPush,
    arrayMove,
    arrayDelete,
    setValues,
    updateValues,
    $values,
  } = form;

  // Добавление элемента в начало массива
  sample({
    clock: arrayUnshift,
    source: $values,
    fn: (values, { path, value }) => ({
      path,
      value: [value, ...getIn(values, path)],
    }),
    target: setValue,
  });

  // Добавление элемента в конец массива
  sample({
    clock: arrayPush,
    source: $values,
    fn: (values, { path, value }) => ({
      path,
      value: [...getIn(values, path), value],
    }),
    target: setValue,
  });

  // Удаление элемента
  sample({
    clock: arrayDelete,
    source: $values,
    fn: (values, { path, index }) => {
      const array = [...getIn(values, path)];

      array.splice(index, 1);

      return {
        path,
        value: array,
      };
    },
    target: setValue,
  });

  // Изменение индекса элемента
  sample({
    clock: arrayMove,
    source: $values,
    fn: (values, { path, source, target }) => {
      const fieldArray = [...getIn(values, path)];
      const element = fieldArray[source];
      fieldArray.splice(source, 1);
      fieldArray.splice(target, 0, element);
      return { path, value: fieldArray };
    },
    target: setValue,
  });
};
