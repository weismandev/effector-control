import { debug } from 'patronum';

import { FormConfig, FormUnits } from '@/src';

export const debugModule = <V>(form: FormUnits<V>, config?: FormConfig<V>) => {
  /* c8 ignore next 40 */
  if (config?.debug) {
    debug(
      {
        handler: ({ node, value, name, kind }) => {
          const formName = config?.sid ? `📋${config.sid} | ` : '📋';
          const unitType = `[${node.meta.op}] `;
          // eslint-disable-next-line
          console.log(formName + unitType + node.meta.name, value);
        },
      },
      form
    );
  }
};
