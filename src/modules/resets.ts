import { is, sample } from 'effector';
import { reset as patronumReset } from 'patronum';

import { FormConfig, FormUnits } from '@/src';

export const resetsModule = <V>(form: FormUnits<V>, config?: FormConfig<V>) => {
  const {
    initValues,
    $errors,
    $valid,
    $dirty,
    initCurrent,
    reset,
    $initialValues,
    $values,
    $validationSchema,
    $editable,
  } = form;
  const isExternalInitialValues = is.store(config?.initialValues);

  if (isExternalInitialValues) {
    // Сброс метовых сторов
    patronumReset({
      clock: initValues,
      target: [$errors, $valid, $dirty],
    });
  } else {
    // Сброс метовых сторов
    patronumReset({
      clock: [initCurrent, initValues],
      target: [$errors, $valid, $dirty],
    });
  }

  // Установка изначальных значений при сбросе формы
  sample({
    clock: reset,
    source: $initialValues,
    target: $values,
  });

  // Сброс формы по команде
  patronumReset({
    clock: reset,
    target: [$validationSchema, $errors, $editable, $valid, $dirty],
  });
};
