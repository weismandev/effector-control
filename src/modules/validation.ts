import { sample } from 'effector';
import { ZodSchema } from 'zod';

import { FormUnits } from '@/src';
import { zodDeepPick } from '@/src/lib/zodDeepPick';
import { debounce } from 'patronum';

export const validationModule = <V>(form: FormUnits<V>) => {
  const {
    setValidationSchema,
    $validationSchema,
    externalValidate,
    internalValidate,
    $values,
    fxExternalValidate,
    fxInternalValidate,
    fxPathValidate,
    submit,
    submitted,
    $submittable,
    $errors,
    rejected,
    setValue,
  } = form;

  const setValueDebounced = debounce({ source: setValue, timeout: 500 });

  // Установка схемы валидации
  sample({
    clock: setValidationSchema,
    target: $validationSchema,
  });

  // Вызов эффекта валидации при срабатывании ивента
  // Внешняя валидация:
  sample({
    clock: externalValidate,
    source: { validationSchema: $validationSchema, values: $values },
    filter: ({ validationSchema }) => validationSchema !== null,
    fn: ({ validationSchema, values }) => ({
      schema: validationSchema as ZodSchema<Partial<V>>,
      values,
    }),
    target: fxExternalValidate,
  });
  // Внутренняя валидация:
  sample({
    clock: internalValidate,
    source: { validationSchema: $validationSchema, values: $values },
    filter: ({ validationSchema }) => validationSchema !== null,
    fn: ({ validationSchema, values }) => ({
      schema: validationSchema as ZodSchema<Partial<V>>,
      values,
    }),
    target: fxInternalValidate,
  });

  // Валидация после ввода формы
  sample({
    clock: submit,
    source: $validationSchema,
    filter: (validationSchema) => validationSchema !== null,
    target: internalValidate,
  });

  // Вызов результирующего ивента при отсутствии схемы валидации
  sample({
    clock: submit,
    source: { validationSchema: $validationSchema, values: $values },
    filter: ({ validationSchema }) => validationSchema === null,
    fn: ({ values }) => values,
    target: submitted,
  });

  // Вызов результирующего ивента после успешной валидации
  sample({
    clock: fxInternalValidate.done,
    source: $values,
    filter: $submittable,
    target: submitted,
  });

  // Сброс ошибок после успешной валидации
  sample({
    clock: submitted,
    fn: () => ({}),
    target: $errors,
  });

  // Установка ошибок валидации
  sample({
    clock: [
      fxExternalValidate.failData,
      fxInternalValidate.failData,
      fxPathValidate.failData,
    ],
    source: $errors,
    fn: (stateErrors, newErrors) => ({
      ...stateErrors,
      ...newErrors,
    }),
    target: [$errors, rejected],
  });

  /** Валидация при изменении значения */

  sample({
    clock: setValueDebounced,
    source: $validationSchema,
    filter: (validationSchema) => validationSchema !== null,
    fn: (validationSchema, { path, value }) => {
      const schema = zodDeepPick(validationSchema as any, path);

      return {
        schema: schema as ZodSchema<Partial<V>>,
        path,
        value,
      };
    },
    target: fxPathValidate,
  });

  sample({
    clock: fxPathValidate.done,
    source: $errors,
    fn: (errors, { params }) => {
      delete errors[params.path];
      return { ...errors };
    },
    target: $errors,
  });
};
