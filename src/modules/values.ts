import { sample } from 'effector';
import lodashExtend from 'lodash/extend';

import { FormUnits } from '@/src';
import { setIn } from '@/src/lib';

export const valuesModule = <V>(form: FormUnits<V>) => {
  const { setValue, setValues, updateValues, $values } = form;

  // Установка значения
  sample({
    clock: setValue,
    source: $values,
    fn: (state, { path, value }) => ({
      ...setIn(state, path, value),
    }),
    target: $values,
  });

  // Полная установка значений
  sample({
    clock: setValues,
    target: $values,
  });

  // Частичная установка значений
  sample({
    clock: updateValues,
    source: $values,
    fn: (state, values) => ({ ...lodashExtend({}, state, values) }),
    target: $values,
  });
};
