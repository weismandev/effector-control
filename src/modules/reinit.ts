import { is, sample, Store } from 'effector';

import { FormConfig, FormUnits } from '@/src';

export const reinitModule = <V>(form: FormUnits<V>, config?: FormConfig<V>) => {
  const { initValues, initCurrent, $values, $initialValues } = form;
  const isExternalInitialValues = is.store(config?.initialValues);

  if (isExternalInitialValues) {
    // Вызываем ивент при изменении внешнего стора со значениями
    sample({
      clock: config?.initialValues as Store<V>,
      target: initValues,
    });
  } else {
    // Инициализация текущих значений
    sample({
      clock: initCurrent,
      source: $values,
      // fn: (values) => ({ ...values }),
      target: $initialValues,
    });
  }

  // Инициализация значениями
  sample({
    clock: initValues,
    fn: (values) => ({ ...values }),
    target: $initialValues,
  });

  // Установка значений при инициализации
  sample({
    clock: initValues,
    target: $values,
  });
};
