import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from './setup';

test('Базовый', () => {
  const form = createDefaultForm();

  form.setValue({ path: 'name', value: 'newName' });

  const newValues = {
    ...initialValues,
    name: 'newName',
  };

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внутренней инициализации', () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  form.setValue({ path: 'name', value: 'newName' });

  const newValues = {
    ...newInitialValues,
    name: 'newName',
  };

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внешней инициализации', () => {
  const form = createExternalForm();

  setExternalValues();

  form.setValue({ path: 'name', value: 'newName' });

  const newValues = {
    ...newExternalValues,
    name: 'newName',
  };

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('С типом boolean', () => {
  const form = createDefaultForm();

  form.setValue({ path: 'address.actual', value: false });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().address.actual).toBeFalsy();
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('С типом object', () => {
  const form = createDefaultForm();

  const newAddress = {
    city: 'Perm',
    street: 'Pushkin',
    building: 10,
    actual: true,
  };

  form.setValue({
    path: 'address',
    value: newAddress,
  });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject({
    ...initialValues,
    address: newAddress,
  });
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('С типом array', () => {
  const form = createDefaultForm();

  const newBooks = [
    { title: 'Book-1', desc: 'Book-1-desc' },
    { title: 'Book-2', desc: 'Book-2-desc' },
  ];

  form.setValue({
    path: 'books',
    value: newBooks,
  });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual(newBooks);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Path как путь до объекта', () => {
  const form = createDefaultForm();

  form.setValue({ path: 'address.city', value: 'Perm' });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().address.city).toBe('Perm');
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Path как путь до массива', () => {
  const form = createDefaultForm();

  form.setValue({ path: 'books[0].title', value: 'newBookTitle' });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books[0].title).toBe('newBookTitle');
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});
