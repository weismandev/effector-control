import { createEvent } from 'effector';
import { expect, test } from 'vitest';

import { createForm } from '@/src';
import {
  $externalInitialValues,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
  validationSchema,
} from '@/src/core/__test__/setup';

test('Базовый', async () => {
  const testEvent = createEvent();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    validationSchema,
    resetOn: testEvent,
    editable: true,
  });

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: '' });

      form.reset.watch((data) => {
        resolve(data);
      });

      testEvent();
    });

  await promise().then(() => {
    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState()).toMatchObject(initialValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeFalsy();
    expect(form.$submittable.getState()).toBeFalsy();
  });
});

test('Базовый: после внутренней инициализации', async () => {
  const testEvent = createEvent();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    validationSchema,
    resetOn: testEvent,
    editable: true,
  });

  form.setValues(newInitialValues);
  form.initCurrent();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: '' });

      form.reset.watch((data) => {
        resolve(data);
      });

      testEvent();
    });

  await promise().then(() => {
    expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
    expect(form.$values.getState()).toMatchObject(newInitialValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeFalsy();
    expect(form.$submittable.getState()).toBeFalsy();
  });
});

test('Базовый: после внешней инициализации', async () => {
  const testEvent = createEvent();
  const form = createForm<any>({
    initialValues: $externalInitialValues,
    validationSchema,
    resetOn: testEvent,
    editable: true,
  });

  setExternalValues();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: '' });

      form.reset.watch((data) => {
        resolve(data);
      });

      testEvent();
    });

  await promise().then(() => {
    expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
    expect(form.$values.getState()).toMatchObject(newExternalValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeFalsy();
    expect(form.$submittable.getState()).toBeFalsy();
  });
});

test('Массив юнитов', async () => {
  const firstTestEvent = createEvent();
  const secondTestEvent = createEvent();

  const form = createForm<any>({
    initialValues: { ...initialValues },
    validationSchema,
    resetOn: [firstTestEvent, secondTestEvent],
    editable: true,
  });

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: '' });

      form.reset.watch((data) => {
        resolve(data);
      });

      secondTestEvent();
    });

  await promise().then(() => {
    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState()).toMatchObject(initialValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeFalsy();
    expect(form.$submittable.getState()).toBeFalsy();
  });
});
