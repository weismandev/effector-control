import { createEvent } from 'effector';
import { expect, test } from 'vitest';

import { createForm } from '@/src';
import {
  $externalInitialValues,
  initialValues,
  newInitialValues,
  setExternalValues,
} from '@/src/core/__test__/setup';

test('Базовый', async () => {
  const testEvent = createEvent<any>();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    initValuesOn: testEvent,
    editable: true,
  });

  form.setValues({});

  testEvent(newInitialValues);

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Базовый: после внутренней инициализации', async () => {
  const testEvent = createEvent<any>();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    initValuesOn: testEvent,
    editable: true,
  });

  form.setValues({});
  form.initCurrent();

  testEvent(newInitialValues);

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Базовый: после внешней инициализации', async () => {
  const testEvent = createEvent<any>();
  const form = createForm<any>({
    initialValues: $externalInitialValues,
    initValuesOn: testEvent,
    editable: true,
  });

  setExternalValues();

  form.setValues({});

  testEvent(newInitialValues);

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Массив юнитов', async () => {
  const firstTestEvent = createEvent<any>();
  const secondTestEvent = createEvent<any>();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    initValuesOn: [firstTestEvent, secondTestEvent],
    editable: true,
  });

  form.setValues({});

  secondTestEvent(newInitialValues);

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});
