import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  setExternalValues,
  validationSchema,
} from './setup';

test('С текущими значениями', () => {
  const form = createDefaultForm();

  const newValues = {
    ...initialValues,
    name: 'newName',
    books: [{ title: 'newTitle', desc: 'newDesc' }],
  };

  form.setValues(newValues);

  form.initCurrent();

  expect(form.$initialValues.getState()).toMatchObject(newValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('С установкой значений', () => {
  const form = createDefaultForm();

  const newValues = {
    ...initialValues,
    name: 'newName',
    books: [{ title: 'newTitle', desc: 'newDesc' }],
  };

  form.initValues(newValues);

  expect(form.$initialValues.getState()).toMatchObject(newValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  4;
  expect(form.$submittable.getState()).toBeFalsy();
});

test('С внешним стором', () => {
  const form = createExternalForm();

  setExternalValues();

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newExternalValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});
