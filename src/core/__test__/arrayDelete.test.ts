import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from './setup';

test('Базовый', () => {
  const form = createDefaultForm();

  form.arrayDelete({ path: 'books', index: 0 });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внутренней инициализации', () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  form.arrayDelete({ path: 'books', index: 0 });

  const newValues = {
    ...newInitialValues,
    books: [],
  };

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внешней инициализации', () => {
  const form = createExternalForm();

  setExternalValues();

  form.arrayDelete({ path: 'books', index: 0 });

  const newValues = {
    ...newExternalValues,
    books: [],
  };

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});
