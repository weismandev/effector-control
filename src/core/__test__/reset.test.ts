import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
  validationSchema,
} from './setup';

test('Базовый', async () => {
  const form = createDefaultForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 'newName' });

      form.reset();

      resolve(form);
    });

  await promise().then(() => {
    expect(form.$values.getState()).toMatchObject(initialValues);
    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeFalsy();
  });
});

test('Базовый: после внутренней инициализации', async () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 'newName' });

      form.reset();

      resolve(form);
    });

  await promise().then(() => {
    expect(form.$values.getState()).toMatchObject(newInitialValues);
    expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeFalsy();
  });
});

test('Базовый: после внешней инициализации', async () => {
  const form = createExternalForm();

  setExternalValues();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 'newName' });

      form.reset();

      resolve(form);
    });

  await promise().then(() => {
    expect(form.$values.getState()).toMatchObject(newExternalValues);
    expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeFalsy();
  });
});

test('После инициализации', async () => {
  const form = createDefaultForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 'newName' });

      form.initCurrent();

      form.setValue({ path: 'name', value: 'customName' });

      form.reset();

      resolve(form);
    });

  await promise().then(() => {
    expect(form.$values.getState()).toMatchObject({
      ...initialValues,
      name: 'newName',
    });
    expect(form.$initialValues.getState()).toMatchObject({
      ...initialValues,
      name: 'newName',
    });
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeFalsy();
  });
});
