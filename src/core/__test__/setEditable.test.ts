import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
  validationSchema,
} from './setup';

test('Отрицательный', () => {
  const form = createDefaultForm();

  form.setEditable(false);

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeFalsy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Отрицательный: после внутренней инициализации', () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  form.setEditable(false);

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeFalsy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Отрицательный: после внешней инициализации', () => {
  const form = createExternalForm();

  setExternalValues();

  form.setEditable(false);

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newExternalValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeFalsy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Положительный', () => {
  const form = createDefaultForm();

  form.setEditable(true);

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Неправильный тип аргумента', () => {
  const form = createDefaultForm();

  // @ts-ignore
  form.setEditable(50);

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});
