import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from './setup';

test('Значение число', () => {
  const form = createDefaultForm();

  form.arrayPush({ path: 'books', value: 50 });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
    50,
  ]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение число: после внутренней инициализации', () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  form.arrayPush({ path: 'books', value: 50 });

  const newValues = {
    ...newInitialValues,
    books: [
      {
        title: 'bookTitle',
        desc: 'bookDesc',
      },
      50,
    ],
  };

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение число: после внешней инициализации', () => {
  const form = createExternalForm();

  setExternalValues();

  form.arrayPush({ path: 'books', value: 50 });

  const newValues = {
    ...newExternalValues,
    books: [
      {
        title: 'bookTitle',
        desc: 'bookDesc',
      },
      50,
    ],
  };

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение строка', () => {
  const form = createDefaultForm();

  form.arrayPush({ path: 'books', value: '50' });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
    '50',
  ]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение boolean', () => {
  const form = createDefaultForm();

  form.arrayPush({ path: 'books', value: true });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
    true,
  ]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение объект', () => {
  const form = createDefaultForm();

  form.arrayPush({ path: 'books', value: { title: '', desc: '' } });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
    { title: '', desc: '' },
  ]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Неправильный тип path', () => {
  const form = createDefaultForm();

  // @ts-ignore
  form.arrayPush({ path: 50, value: 10 });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Конечный путь не массив', () => {
  const form = createDefaultForm();

  form.arrayPush({ path: 'address', value: true });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});
