import { expect, test } from 'vitest';

import { createForm } from '@/src';

import {
  createDefaultForm,
  createExternalForm,
  FormData,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from './setup';

test('Базовый', async () => {
  const form = createDefaultForm();

  const promise = () =>
    new Promise((resolve) => {
      form.submitted.watch((values: any) => {
        resolve(values);
      });

      form.setValue({ path: 'name', value: 'newName' });

      form.submit();
    });

  await promise().then((values) => {
    expect(values).toMatchObject({ ...initialValues, name: 'newName' });
    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState()).toMatchObject({
      ...initialValues,
      name: 'newName',
    });
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Базовый: после внутренней инициализации', async () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  const promise = () =>
    new Promise((resolve) => {
      form.submitted.watch((values: any) => {
        resolve(values);
      });

      form.setValue({ path: 'name', value: 'newName' });

      form.submit();
    });

  await promise().then((values) => {
    expect(values).toMatchObject({ ...newInitialValues, name: 'newName' });
    expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
    expect(form.$values.getState()).toMatchObject({
      ...newInitialValues,
      name: 'newName',
    });
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Базовый: после внешней инициализации', async () => {
  const form = createExternalForm();

  setExternalValues();

  const promise = () =>
    new Promise((resolve) => {
      form.submitted.watch((values: any) => {
        resolve(values);
      });

      form.setValue({ path: 'name', value: 'newName' });

      form.submit();
    });

  await promise().then((values) => {
    expect(values).toMatchObject({ ...newExternalValues, name: 'newName' });
    expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
    expect(form.$values.getState()).toMatchObject({
      ...newExternalValues,
      name: 'newName',
    });
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('При отсутствии схемы валидации', async () => {
  const form = createForm<FormData>({
    initialValues: { ...initialValues },
    editable: true,
  });

  const promise = () =>
    new Promise((resolve) => {
      form.submitted.watch((values: any) => {
        resolve(values);
      });

      form.setValue({ path: 'name', value: 'newName' });

      form.submit();
    });

  await promise().then((values) => {
    expect(values).toMatchObject({ ...initialValues, name: 'newName' });
    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState()).toMatchObject({
      ...initialValues,
      name: 'newName',
    });
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('При неудачном вводе', async () => {
  const form = createDefaultForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: null });

      form.submit();

      form.$errors.watch((errors) => {
        if (Object.keys(errors).length) {
          resolve(errors);
        }
      });
    });

  await promise().then(() => {
    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState()).toMatchObject({
      ...initialValues,
      name: null,
    });
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toHaveProperty('name');
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeFalsy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});
