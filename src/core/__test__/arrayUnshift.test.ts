import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from './setup';

test('Значение число', () => {
  const form = createDefaultForm();

  form.arrayUnshift({ path: 'books', value: 50 });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([
    50,
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение число:  после внутренней инициализации', () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  form.arrayUnshift({ path: 'books', value: 50 });

  const newValues = {
    ...newInitialValues,
    books: [
      50,
      {
        title: 'bookTitle',
        desc: 'bookDesc',
      },
    ],
  };

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение число: после внешней инициализации', () => {
  const form = createExternalForm();

  setExternalValues();

  form.arrayUnshift({ path: 'books', value: 50 });

  const newValues = {
    ...newExternalValues,
    books: [
      50,
      {
        title: 'bookTitle',
        desc: 'bookDesc',
      },
    ],
  };

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение строка', () => {
  const form = createDefaultForm();

  form.arrayUnshift({ path: 'books', value: '50' });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([
    '50',
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение boolean ', () => {
  const form = createDefaultForm();

  form.arrayUnshift({ path: 'books', value: true });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([
    true,
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Значение объект', () => {
  const form = createDefaultForm();

  form.arrayUnshift({ path: 'books', value: { title: '', desc: '' } });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([
    { title: '', desc: '' },
    { desc: 'bookDesc', title: 'bookTitle' },
  ]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Неправильный тип path', () => {
  const form = createDefaultForm();

  // @ts-ignore
  form.arrayUnshift({ path: 50, value: 10 });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual(initialValues.books);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Конечный путь не массив', () => {
  const form = createDefaultForm();

  form.arrayUnshift({ path: 'address', value: true });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});
