import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from './setup';

test('Базовый', () => {
  const form = createDefaultForm();
  const newArrayItem = { title: 'testTitle', desc: 'testDesc' };

  form.arrayUnshift({ path: 'books', value: newArrayItem });
  form.arrayMove({ path: 'books', source: 0, target: 1 });

  const newValues = {
    ...initialValues,
    books: [...initialValues.books, newArrayItem],
  };

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внутренней инициализации', () => {
  const form = createDefaultForm();
  const newArrayItem = { title: 'testTitle', desc: 'testDesc' };

  form.setValues(newInitialValues);
  form.initCurrent();

  form.arrayUnshift({ path: 'books', value: newArrayItem });
  form.arrayMove({ path: 'books', source: 0, target: 1 });

  const newValues = {
    ...newInitialValues,
    books: [...newInitialValues.books, newArrayItem],
  };

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внешней инициализации', () => {
  const form = createExternalForm();
  const newArrayItem = { title: 'testTitle', desc: 'testDesc' };

  setExternalValues();

  form.setValues(newExternalValues);
  form.initCurrent();

  form.arrayUnshift({ path: 'books', value: newArrayItem });
  form.arrayMove({ path: 'books', source: 0, target: 1 });

  const newValues = {
    ...newExternalValues,
    books: [...newExternalValues.books, newArrayItem],
  };

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Неправильный тип path', () => {
  const form = createDefaultForm();

  // @ts-ignore
  form.arrayMove({ path: 10, source: 0, target: 1 });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('С равными параметрами source и target', () => {
  const form = createDefaultForm();
  const newArrayItem = { title: 'testTitle', desc: 'testDesc' };

  form.arrayUnshift({ path: 'books', value: newArrayItem });
  form.arrayMove({ path: 'books', source: 0, target: 0 });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toEqual([newArrayItem, initialValues.books[0]]);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});
