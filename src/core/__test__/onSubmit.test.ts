import { createEvent } from 'effector';
import { expect, test } from 'vitest';

import { createForm } from '@/src';
import {
  $externalInitialValues,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
  validationSchema,
} from '@/src/core/__test__/setup';

test('Базовый', async () => {
  const testEvent = createEvent();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    validationSchema,
    onSubmit: testEvent,
    editable: true,
  });

  const newValues = {
    ...initialValues,
    name: 'newName',
  };

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 'newName' });
      form.submit();

      testEvent.watch((data) => {
        resolve(data);
      });
    });

  await promise().then((data) => {
    expect(data).toMatchObject(newValues);

    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState()).toMatchObject(newValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Базовый: после внутренней инициализации', async () => {
  const testEvent = createEvent();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    validationSchema,
    onSubmit: testEvent,
    editable: true,
  });

  form.setValues(newInitialValues);
  form.initCurrent();

  const newValues = {
    ...newInitialValues,
    name: 'newName',
  };

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 'newName' });
      form.submit();

      testEvent.watch((data) => {
        resolve(data);
      });
    });

  await promise().then((data) => {
    expect(data).toMatchObject(newValues);

    expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
    expect(form.$values.getState()).toMatchObject(newValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Базовый: после внешней инициализации', async () => {
  const testEvent = createEvent();
  const form = createForm<any>({
    initialValues: $externalInitialValues,
    validationSchema,
    onSubmit: testEvent,
    editable: true,
  });

  setExternalValues();

  const newValues = {
    ...newExternalValues,
    name: 'newName',
  };

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 'newName' });
      form.submit();

      testEvent.watch((data) => {
        resolve(data);
      });
    });

  await promise().then((data) => {
    expect(data).toMatchObject(newValues);

    expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
    expect(form.$values.getState()).toMatchObject(newValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Массив юнитов', async () => {
  const firstTestEvent = createEvent();
  const secondTestEvent = createEvent();

  const form = createForm<any>({
    initialValues: { ...initialValues },
    validationSchema,
    onSubmit: [firstTestEvent, secondTestEvent],
    editable: true,
  });

  const newValues = {
    ...initialValues,
    name: 'newName',
  };

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: 'newName' });
      form.submit();

      secondTestEvent.watch((data) => {
        resolve(data);
      });
    });

  await promise().then((data) => {
    expect(data).toMatchObject(newValues);

    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState()).toMatchObject(newValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toMatchObject({});
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeTruthy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});
