import { createEvent } from 'effector';
import { expect, test } from 'vitest';

import { createForm } from '@/src';
import {
  $externalInitialValues,
  initialValues,
  newInitialValues,
  setExternalValues,
} from '@/src/core/__test__/setup';

test('Базовый', async () => {
  const testEvent = createEvent();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    initCurrentOn: testEvent,
    editable: true,
  });

  form.setValues(newInitialValues);

  testEvent();

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Базовый: после внутренней инициализации', async () => {
  const testEvent = createEvent();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    initCurrentOn: testEvent,
    editable: true,
  });

  form.setValue({ path: 'name', value: 'testName' });
  form.initCurrent();

  form.setValues(newInitialValues);

  testEvent();

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Базовый: после внешней инициализации', async () => {
  const testEvent = createEvent();
  const form = createForm<any>({
    initialValues: $externalInitialValues,
    initCurrentOn: testEvent,
    editable: true,
  });

  setExternalValues();

  form.setValues(newInitialValues);

  testEvent();

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Массив юнитов', async () => {
  const firstTestEvent = createEvent();
  const secondTestEvent = createEvent();
  const form = createForm<any>({
    initialValues: { ...initialValues },
    initCurrentOn: [firstTestEvent, secondTestEvent],
    editable: true,
  });

  form.setValues(newInitialValues);

  secondTestEvent();

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});
