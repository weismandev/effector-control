import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from './setup';

test('Базовый', () => {
  const form = createDefaultForm();

  const newValues = {
    ...initialValues,
    name: 'newName',
    books: [{ title: 'newTitle', desc: 'newDesc' }],
  };

  form.setValues(newValues);

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внутренней инициализации', () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  const newValues = {
    ...newInitialValues,
    name: 'newName',
    books: [{ title: 'newTitle', desc: 'newDesc' }],
  };

  form.setValues(newValues);

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внешней инициализации', () => {
  const form = createExternalForm();

  setExternalValues();

  const newValues = {
    ...newExternalValues,
    name: 'newName',
    books: [{ title: 'newTitle', desc: 'newDesc' }],
  };

  form.setValues(newValues);

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Пустой объект', () => {
  const form = createDefaultForm();

  // @ts-ignore
  form.setValues({});

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject({});
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});
