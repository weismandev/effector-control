import { createEvent, createStore, sample } from 'effector';
import * as Zod from 'zod';

import { createForm } from '@/src';

export type FormData = {
  name: string;
  surname?: string;
  address: {
    city: string;
    street?: string;
    building?: number;
    actual?: boolean;
  };
  books:
    | {
        title: string;
        desc: string;
      }[]
    | [];
};

export const validationSchema = Zod.object({
  name: Zod.string().nonempty(),
  surname: Zod.string().nonempty(),
  address: Zod.object({
    city: Zod.string().nonempty(),
    street: Zod.string().nonempty(),
    building: Zod.number(),
    actual: Zod.boolean(),
  }),
}).refine((data) => !(data.name && !data.surname), {
  message: 'Surname required if name already exist',
  path: ['name'],
});

export const initialValues: FormData = {
  name: 'name',
  surname: 'surname',
  address: {
    city: 'Moscow',
    street: 'Lenin',
    building: 5,
    actual: true,
  },
  books: [
    {
      title: 'bookTitle',
      desc: 'bookDesc',
    },
  ],
};

export const newInitialValues = {
  ...initialValues,
  surname: 'newSurname',
  address: {
    ...initialValues.address,
    city: 'Perm',
  },
};

export const createDefaultForm = () =>
  createForm<FormData>({
    initialValues: { ...initialValues },
    validationSchema,
    editable: true,
  });

/** Внешний стор */

export const $externalInitialValues = createStore<FormData>({ ...initialValues });

export const newExternalValues = {
  ...initialValues,
  surname: 'newSurname',
  address: {
    ...initialValues.address,
    city: 'Perm',
  },
};

export const setExternalValues = createEvent();

sample({
  clock: setExternalValues,
  fn: () => newExternalValues,
  target: $externalInitialValues,
});

export const createExternalForm = () =>
  createForm<FormData>({
    initialValues: $externalInitialValues,
    validationSchema,
    editable: true,
  });
