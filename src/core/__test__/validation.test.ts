import { expect, test } from 'vitest';

import { createForm } from '@/src';

import {
  createDefaultForm,
  createExternalForm,
  FormData,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from './setup';

test('Базовый', async () => {
  const form = createDefaultForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: '' });

      form.submit();

      form.$errors.watch((errors) => {
        if (Object.keys(errors).length) {
          resolve(errors);
        }
      });
    });

  await promise().then((errors) => {
    expect(errors).toHaveProperty('name');

    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState().name).toBe('');
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState().name).toEqual(
      'String must contain at least 1 character(s)'
    );
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeFalsy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Базовый: после внутренней инициализации', async () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: '' });

      form.submit();

      form.$errors.watch((errors) => {
        if (Object.keys(errors).length) {
          resolve(errors);
        }
      });
    });

  const newValues = {
    ...newInitialValues,
    name: '',
  };

  await promise().then((errors) => {
    expect(errors).toHaveProperty('name');

    expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
    expect(form.$values.getState()).toMatchObject(newValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState().name).toEqual(
      'String must contain at least 1 character(s)'
    );
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeFalsy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Базовый: после внешней инициализации', async () => {
  const form = createExternalForm();

  setExternalValues();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: '' });

      form.submit();

      form.$errors.watch((errors) => {
        if (Object.keys(errors).length) {
          resolve(errors);
        }
      });
    });

  const newValues = {
    ...newExternalValues,
    name: '',
  };

  await promise().then((errors) => {
    expect(errors).toHaveProperty('name');

    expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
    expect(form.$values.getState()).toMatchObject(newValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState().name).toEqual(
      'String must contain at least 1 character(s)'
    );
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeFalsy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Зависимость от других полей', async () => {
  const form = createDefaultForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'surname', value: '' });

      form.submit();

      form.$errors.watch((errors) => {
        if (Object.keys(errors).length) {
          resolve(errors);
        }
      });
    });

  const newValues = {
    ...initialValues,
    surname: '',
  };

  await promise().then((errors) => {
    expect(errors).toHaveProperty('name');

    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState()).toMatchObject(newValues);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toHaveProperty('name');
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeFalsy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Принудительный вызов validate', async () => {
  const form = createDefaultForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: '' });

      form.validate();

      form.$errors.watch((errors) => {
        if (Object.keys(errors).length) {
          resolve(errors);
        }
      });
    });

  await promise().then((errors) => {
    expect(errors).toHaveProperty('name');

    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState().name).toBe('');
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState().name).toEqual(
      'String must contain at least 1 character(s)'
    );
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeFalsy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Без схемы валидации', () => {
  const form = createForm<FormData>({
    initialValues: { ...initialValues },
    editable: true,
  });

  form.setValue({ path: 'name', value: '' });

  form.submit();

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().name).toBe('');
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Неправильный тип значения', async () => {
  const form = createDefaultForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: null });

      form.submit();

      form.$errors.watch((errors) => {
        if (Object.keys(errors).length) {
          resolve(errors);
        }
      });
    });

  await promise().then((errors) => {
    expect(errors).toHaveProperty('name');

    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState().name).toBe(null);
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState()).toHaveProperty('name');
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeFalsy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});

test('Внешняя валидация', async () => {
  const form = createDefaultForm();

  const promise = () =>
    new Promise((resolve) => {
      form.setValue({ path: 'name', value: null });

      form.validate();

      form.$errors.watch((errors) => {
        if (Object.keys(errors).length) {
          resolve(errors);
        }
      });
    });

  await promise().then((errors) => {
    expect(errors).toHaveProperty('name');

    expect(form.$initialValues.getState()).toMatchObject(initialValues);
    expect(form.$values.getState().name).toBe('');
    expect(form.$validationSchema.getState()).toMatchObject({});
    expect(form.$errors.getState().name).toEqual(
      'String must contain at least 1 character(s)'
    );
    expect(form.$editable.getState()).toBeTruthy();
    expect(form.$valid.getState()).toBeFalsy();
    expect(form.$dirty.getState()).toBeTruthy();
    expect(form.$submittable.getState()).toBeTruthy();
  });
});
