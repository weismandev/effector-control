import { is } from 'effector';
import { expect, test } from 'vitest';

import { createForm } from '@/src';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  validationSchema,
} from './setup';

test('Корректное создание формы', () => {
  const form = createDefaultForm();
  expect(typeof form).toBe('object');
});

test('Корректность юнитов', () => {
  const form = createDefaultForm();

  expect(is.store(form.$initialValues)).toBeTruthy();
  expect(is.store(form.$validationSchema)).toBeTruthy();
  expect(is.store(form.$values)).toBeTruthy();
  expect(is.store(form.$errors)).toBeTruthy();
  expect(is.store(form.$editable)).toBeTruthy();
  expect(is.store(form.$valid)).toBeTruthy();
  expect(is.store(form.$dirty)).toBeTruthy();
  expect(is.store(form.$submittable)).toBeTruthy();
  expect(is.event(form.submit)).toBeTruthy();
  expect(is.event(form.submitted)).toBeTruthy();
  expect(is.event(form.reset)).toBeTruthy();
  expect(is.event(form.initCurrent)).toBeTruthy();
  expect(is.event(form.initValues)).toBeTruthy();
  expect(is.event(form.setEditable)).toBeTruthy();
  expect(is.event(form.setValue)).toBeTruthy();
  expect(is.event(form.setValues)).toBeTruthy();
  expect(is.event(form.arrayPush)).toBeTruthy();
  expect(is.event(form.arrayDelete)).toBeTruthy();
  expect(is.event(form.arrayMove)).toBeTruthy();
  expect(is.event(form.setValidationSchema)).toBeTruthy();
});

test('Корректность юнитов без аргументов', () => {
  const form = createForm();

  expect(is.store(form.$initialValues)).toBeTruthy();
  expect(is.store(form.$validationSchema)).toBeTruthy();
  expect(is.store(form.$values)).toBeTruthy();
  expect(is.store(form.$errors)).toBeTruthy();
  expect(is.store(form.$editable)).toBeTruthy();
  expect(is.store(form.$valid)).toBeTruthy();
  expect(is.store(form.$dirty)).toBeTruthy();
  expect(is.store(form.$submittable)).toBeTruthy();
  expect(is.event(form.submit)).toBeTruthy();
  expect(is.event(form.submitted)).toBeTruthy();
  expect(is.event(form.reset)).toBeTruthy();
  expect(is.event(form.initCurrent)).toBeTruthy();
  expect(is.event(form.initValues)).toBeTruthy();
  expect(is.event(form.setEditable)).toBeTruthy();
  expect(is.event(form.setValue)).toBeTruthy();
  expect(is.event(form.setValues)).toBeTruthy();
  expect(is.event(form.arrayPush)).toBeTruthy();
  expect(is.event(form.arrayDelete)).toBeTruthy();
  expect(is.event(form.arrayMove)).toBeTruthy();
  expect(is.event(form.setValidationSchema)).toBeTruthy();
});

test('Правильные значения юнитов', () => {
  const form = createDefaultForm();

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Правильные значения юнитов без аргументов', () => {
  const form = createForm();

  expect(form.$initialValues.getState()).toMatchObject({});
  expect(form.$values.getState()).toMatchObject({});
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Корректное создание формы с внешним стором', () => {
  const form = createExternalForm();
  expect(typeof form).toBe('object');
});

test('Корректность юнитов', () => {
  const form = createDefaultForm();

  expect(is.store(form.$initialValues)).toBeTruthy();
  expect(is.store(form.$validationSchema)).toBeTruthy();
  expect(is.store(form.$values)).toBeTruthy();
  expect(is.store(form.$errors)).toBeTruthy();
  expect(is.store(form.$editable)).toBeTruthy();
  expect(is.store(form.$valid)).toBeTruthy();
  expect(is.store(form.$dirty)).toBeTruthy();
  expect(is.store(form.$submittable)).toBeTruthy();
  expect(is.event(form.submit)).toBeTruthy();
  expect(is.event(form.submitted)).toBeTruthy();
  expect(is.event(form.reset)).toBeTruthy();
  expect(is.event(form.initCurrent)).toBeTruthy();
  expect(is.event(form.initValues)).toBeTruthy();
  expect(is.event(form.setEditable)).toBeTruthy();
  expect(is.event(form.setValue)).toBeTruthy();
  expect(is.event(form.setValues)).toBeTruthy();
  expect(is.event(form.arrayPush)).toBeTruthy();
  expect(is.event(form.arrayDelete)).toBeTruthy();
  expect(is.event(form.arrayMove)).toBeTruthy();
  expect(is.event(form.setValidationSchema)).toBeTruthy();
});

test('Правильные значения юнитов c внешним стором', () => {
  const form = createExternalForm();

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});
