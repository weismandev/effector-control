import { expect, test } from 'vitest';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
  validationSchema,
} from './setup';

test('Базовый', () => {
  const form = createDefaultForm();

  form.updateValues({
    name: 'newName',
  });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject({
    ...initialValues,
    name: 'newName',
  });
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внутренней инициализации', () => {
  const form = createDefaultForm();

  form.setValues(newInitialValues);
  form.initCurrent();

  form.updateValues({
    name: 'newName',
  });

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject({
    ...newInitialValues,
    name: 'newName',
  });
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внешней инициализации', () => {
  const form = createExternalForm();

  setExternalValues();

  form.updateValues({
    name: 'newName',
  });

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject({
    ...newExternalValues,
    name: 'newName',
  });
  expect(form.$validationSchema.getState()).toMatchObject(validationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Объект', () => {
  const form = createDefaultForm();

  const newAddress = {
    city: 'Perm',
    street: 'Pushkin',
    building: 8,
    actual: true,
  };

  form.updateValues({
    address: newAddress,
  });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject({
    ...initialValues,
    address: newAddress,
  });
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Часть объекта', () => {
  const form = createDefaultForm();

  const newAddress = { city: 'Perm' };

  form.updateValues({
    address: newAddress,
  });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().address).toMatchObject(newAddress);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Часть массива', () => {
  const form = createDefaultForm();

  const newBooks = [
    {
      title: 'newTitle',
      desc: 'newDesc',
    },
  ];

  form.updateValues({
    books: newBooks,
  });

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState().books).toBe(newBooks);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Пустой объект', () => {
  const form = createDefaultForm();

  form.updateValues({});

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toMatchObject({});
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});
