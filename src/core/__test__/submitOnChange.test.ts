import { expect, test } from 'vitest';

import { createForm } from '@/src';
import {
  $externalInitialValues,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from '@/src/core/__test__/setup';

test('Базовый', async () => {
  const form = createForm<any>({
    initialValues: { ...initialValues },
    submitOnChange: true,
    editable: true,
  });

  form.setValues(newInitialValues);

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внутренней инициализации', async () => {
  const form = createForm<any>({
    initialValues: { ...initialValues },
    submitOnChange: true,
    editable: true,
  });

  form.setValue({ path: 'name', value: 'testName' });
  form.initCurrent();

  form.setValues(newInitialValues);

  expect(form.$initialValues.getState()).toMatchObject({
    ...initialValues,
    name: 'testName',
  });
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});

test('Базовый: после внешней инициализации', async () => {
  const form = createForm<any>({
    initialValues: $externalInitialValues,
    submitOnChange: true,
    editable: true,
  });

  setExternalValues();

  form.setValue({ path: 'name', value: 'asd' });

  const newValues = {
    ...newExternalValues,
    name: 'asd',
  };

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeTruthy();
  expect(form.$submittable.getState()).toBeTruthy();
});
