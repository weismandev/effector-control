import { expect, test } from 'vitest';
import * as Zod from 'zod';

import {
  createDefaultForm,
  createExternalForm,
  initialValues,
  newExternalValues,
  newInitialValues,
  setExternalValues,
} from './setup';

test('Базовый', () => {
  const form = createDefaultForm();
  const customValidationSchema = Zod.object({
    name: Zod.string().min(5),
  });

  form.setValidationSchema(customValidationSchema);

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  // expect(form.$validationSchema.getState()).toEqual(customValidationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Базовый: после внутренней инициализации', () => {
  const form = createDefaultForm();
  const customValidationSchema = Zod.object({
    name: Zod.string().min(5),
  });

  form.setValues(newInitialValues);
  form.initCurrent();

  form.setValidationSchema(customValidationSchema);

  expect(form.$initialValues.getState()).toMatchObject(newInitialValues);
  expect(form.$values.getState()).toMatchObject(newInitialValues);
  // expect(form.$validationSchema.getState()).toEqual(customValidationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Базовый: после внешней инициализации', () => {
  const form = createExternalForm();
  const customValidationSchema = Zod.object({
    name: Zod.string().min(5),
  });

  setExternalValues();

  form.setValidationSchema(customValidationSchema);

  expect(form.$initialValues.getState()).toMatchObject(newExternalValues);
  expect(form.$values.getState()).toMatchObject(newExternalValues);
  // expect(form.$validationSchema.getState()).toEqual(customValidationSchema);
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});

test('Неправильный тип аргумента (null)', () => {
  const form = createDefaultForm();

  form.setValidationSchema(null);

  expect(form.$initialValues.getState()).toMatchObject(initialValues);
  expect(form.$values.getState()).toMatchObject(initialValues);
  expect(form.$validationSchema.getState()).toBeNull();
  expect(form.$errors.getState()).toMatchObject({});
  expect(form.$editable.getState()).toBeTruthy();
  expect(form.$valid.getState()).toBeTruthy();
  expect(form.$dirty.getState()).toBeFalsy();
  expect(form.$submittable.getState()).toBeFalsy();
});
