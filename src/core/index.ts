import { combine, createEffect, createEvent, createStore, is } from 'effector';
import isEqual from 'fast-deep-equal';
import lodashOmit from 'lodash/omit';

import { validateSchema } from '@/src/lib/validateSchema';
import { validatePath } from '@/src/lib/validatePath';
import { arraysModule } from '@/src/modules/arrays';
import { debugModule } from '@/src/modules/debug';
import { listenerModule } from '@/src/modules/listener';
import { reinitModule } from '@/src/modules/reinit';
import { resetsModule } from '@/src/modules/resets';
import { stateModule } from '@/src/modules/state';
import { validationModule } from '@/src/modules/validation';
import { valuesModule } from '@/src/modules/values';

import {
  FormConfig,
  FormErrors,
  FormInstance,
  FormPathValidationParams,
  FormUnits,
  FormValidationParams,
} from '../types/common';

export const createForm = <V>(config?: FormConfig<V>): FormInstance<V> => {
  /** ---- Сторы ---- */
  // eslint-disable-next-line
  const isExternalInitialValues = is.store(config?.initialValues);

  /* c8 ignore next 13 */
  const $initialValues: FormUnits<V>['$initialValues'] = createStore(
    isExternalInitialValues
      ? // @ts-ignore
        config?.initialValues.getState()
      : config?.initialValues || {}
  );
  const $values: FormUnits<V>['$values'] = createStore($initialValues.getState());
  const $validationSchema: FormUnits<V>['$validationSchema'] = createStore(
    config?.validationSchema || null
  );
  const $errors: FormUnits<V>['$errors'] = createStore(
    {},
    {
      updateFilter: (update, current) =>
        Object.keys(update).length ? !isEqual(update, current) : true,
    }
  );
  const $editable: FormUnits<V>['$editable'] = createStore(
    config && Object.prototype.hasOwnProperty.call(config, 'editable')
      ? (config.editable as boolean)
      : true,
    {
      updateFilter: (update, current) => update !== current,
    }
  );
  const $valid: FormUnits<V>['$valid'] = createStore(true, {
    updateFilter: (update, current) => update !== current,
  });
  const $dirty: FormUnits<V>['$dirty'] = createStore(false, {
    updateFilter: (update, current) => update !== current,
  });
  const $submittable: FormUnits<V>['$submittable'] = combine(
    $editable,
    $dirty,
    (editable, dirty) => editable && dirty
  );

  /** ---- КОМАНДЫ ---- */

  const submit: FormUnits<V>['submit'] = createEvent();
  const reset: FormUnits<V>['reset'] = createEvent();
  const initCurrent: FormUnits<V>['initCurrent'] = createEvent();
  const initValues: FormUnits<V>['initValues'] = createEvent();
  const setEditable: FormUnits<V>['setEditable'] = createEvent();
  const setValue: FormUnits<V>['setValue'] = createEvent();
  const setValues: FormUnits<V>['setValues'] = createEvent();
  const updateValues: FormUnits<V>['updateValues'] = createEvent();
  const arrayUnshift: FormUnits<V>['arrayUnshift'] = createEvent();
  const arrayPush: FormUnits<V>['arrayPush'] = createEvent();
  const arrayDelete: FormUnits<V>['arrayDelete'] = createEvent();
  const arrayMove: FormUnits<V>['arrayMove'] = createEvent();
  const setValidationSchema: FormUnits<V>['setValidationSchema'] = createEvent();
  const externalValidate: FormUnits<V>['externalValidate'] = createEvent();
  const internalValidate: FormUnits<V>['internalValidate'] = createEvent();

  /** ---- События ---- */

  const submitted: FormUnits<V>['submitted'] = createEvent();
  const rejected: FormUnits<V>['rejected'] = createEvent();

  /** ---- События контролов ---- */

  // TODO: здесь можно добавить события для контролов, например blur или focus

  /** ---- Эффекты ---- */

  const fxExternalValidate = createEffect<FormValidationParams<V>, void, FormErrors>(
    validateSchema
  );
  const fxInternalValidate = createEffect<FormValidationParams<V>, void, FormErrors>(
    validateSchema
  );
  const fxPathValidate = createEffect<FormPathValidationParams<V>, void, FormErrors>(
    validatePath
  );

  /** ---- Форма ---- */

  const form: FormUnits<V> = {
    $initialValues,
    $validationSchema,
    $values,
    $errors,
    $editable,
    $valid,
    $dirty,
    $submittable,

    submit,
    reset,
    initCurrent,
    initValues,
    setEditable,
    setValue,
    setValues,
    updateValues,
    arrayUnshift,
    arrayPush,
    arrayDelete,
    arrayMove,
    setValidationSchema,
    externalValidate,
    internalValidate,

    submitted,
    rejected,

    fxExternalValidate,
    fxInternalValidate,
    fxPathValidate,
  };

  /** Подключение модулей */

  valuesModule<V>(form);
  arraysModule<V>(form);
  validationModule<V>(form);
  listenerModule<V>(form, config);
  stateModule<V>(form);
  reinitModule<V>(form, config);
  resetsModule<V>(form, config);
  debugModule<V>(form, config);

  return {
    ...lodashOmit(form, [
      'externalValidate',
      'internalValidate',
      'fxExternalValidate',
      'fxInternalValidate',
    ]),
    initialValues: $initialValues,
    validationSchema: $validationSchema,
    values: $values,
    errors: $errors,
    editable: $editable,
    valid: $valid,
    dirty: $dirty,
    submittable: $submittable,
    validate: externalValidate,
  };
};
