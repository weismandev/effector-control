# Effector form

- [createForm](#createform)
- [Сторы](#сторы)
- [Команды](#команды)
- [События](#события)
- [Рецепты](#рецепты)

## createForm

Фабрика для создания модели формы

### Дженерик

`V` - Тип объекта со значениями

### Аргументы

- `initialValues?`: **V** | Store<V> | **{}** - Исходные значения формы, по умолчанию пустой объект.
  При передаче внешнего стора, будет происходить реинициализация формы
- `validationSchema?`: **Schema\<Partial\<V\>\>** - Схема валидации Zod
- `onSubmit?`: **Unit | Unit[]** - Юниты для успешного ввода формы
- `onReject?`: **Unit | Unit[]** - Юниты для ввода формы с ошибками
- `resetOn?`: **Unit | Unit[]** - Юниты для сброса формы
- `initCurrentOn?`: **Unit | Unit[]** - Юниты для инициализации текущих значений
- `initValuesOn?`: **Unit | Unit[]** - Юниты для инициализации значений
- `submitOnChange`: **boolean** - Режим ввода формы при изменении значений
- `editable?`: **boolean** - Режим редактирования формы, по умолчанию **true**
- `debug?`: **boolean** - Режим отладки
- `sid?`: **string** - Уникальный идентификатор формы, используется в качестве имени домена

### Возвращает

`FormInstance<V>` [Сторы](#сторы) и [Юниты](#события) формы

### Параметры

- `form`:**FormInstance\<V\>** - Форма созданная через фабрику [createForm](#createform)

# Сторы

> У всех сторов есть "дублеры" без знака **$**, для удобства деструктуризация из useUnit.
> В модели рекомендуется использовать сторы только со знаком **$**

- `$values`: **Store\<V\>** - Текущие значения формы
- `$initialValues`: **Store\<V\>** - Исходные значения формы
- `$editable`: **Store\<boolean\>** - Возможность редактировать
- `$valid`: **Store\<boolean\>** - Валидность значений
- `$dirty`: **Store\<boolean\>** - Менялись ли значения относительно исходных
- `$submittable`: **Store\<boolean\>** - Возможность сделать ввод формы. Вычисляемое значение на основе предыдущих
- `$validationSchema`: **Schema\<Partial\<V\>\> | null** - Схема валидации Zod
- `$errors`: **Store\<Record\<string, string\>\>** - Плоский объект с ошибками валидации, формат **\[путь до значения] : текст ошибки**

# Команды

Установка значений

- `setValue`: **Event\<{ path: string; value: any }\>** - Установить значение по указанному пути
- `setValues`: **Event\<V\>** - Заменить текущие значения целиком
- `updateValues`: **Event\<Partial\<V\>\>** - Заменить часть значений
- `arrayPush`: **Event\<{ path: string; structure: Record<string, any> }\>** - Добавить элемент в массив на основе
структуры из вью слоя
- `arrayDelete`: **Event\<{ path: string; index: number }\>** - Удалить элемент массива
- `arrayMove`: **Event\<{ path: string; source: number; target: number }\>** - Переместить элемент массива на другое место

Мета команды

- `submit`: **Event\<void\>** - Ввод формы
- `reset`: **Event\<void\>** - Сброс формы до исходного состояния
- `initCurrent`: **Event\<void\>** - Текущее состояние формы принять за исходное и сбросить все метовые сторы.
**Данный ивент отключается если в качестве initialValues передан внешний стор!**
- `initValues`: **Event\<V\>** - Новые значения принять за исходные и сбросить все метовые сторы
- `setEditable`: **Event\<boolean\>** - Изменить режим редактирования

Валидация

- `setValidationSchema`: **Event\<Schema\<Partial\<V\>\> | null\>**
- `validate`: **Event\<void\>** - Принудительная валидация формы


# События

- `submitted`: **Event\<V\>** - Результирующий ивент после валидации, в него передаются значения из $values
- `rejected`: \*\*Event\<FormErrors\> - Ивент после ввода формы с ошибками

# Рецепты

## Базовое использование

`model`

```ts
import * as Zod from 'zod';
import { createEffect } from 'effector';
import { createForm } from '@unicorn/effector-form';

type FormData = {
  title: string;
  desc: string;
};

const fxSendForm = createEffect<FormData, any, Error>().use(...промис);

// Создаем форму
const myForm = createForm<FormData>({
  initialValues: {
    title: 'example title',
    desc: 'example desc',
  },
  validationSchema: Zod.object({
    title: Zod.string().min(5),
    desc: Zod.string().max(100),
  }),
  onSubmit: fxSendForm,
  initValuesOn: fxSendForm.doneData,
});
```

`view`

```tsx
import { useUnit } from 'effector-react';
import { Button } from '@mui/material';
import { myForm } from './model';

const Page = () => {
  const { dirty, editable, valid, submittable, submit, setEditable, reset } =
    useUnit(myForm);

  return (
    <>
      <Button onCLick={submit} disabled={!submittable}>
        Сохранить
      </Button>

      <Button onClick={reset}>Отменить</Button>

      {editable ? (
        <Button onClick={() => setEditable(false)}>Переключить в режим просмотра</Button>
      ) : (
        <Button onClick={() => testForm.setEditable(true)}>
          Переключить в режим редактирования
        </Button>
      )}

      <MyControl form={myForm} name="myControl" label="My control" />
    </>
  );
};
```

### Использование с внешним стором

`model`

```ts
import * as Zod from 'zod';
import { createEffect, sample } from 'effector';
import { createForm } from '@unicorn/effector-form';

type FormData = {
  title: string;
  desc: string;
};

// Создаем эффект для отправки формы
const fxSendForm = createEffect<FormData, any, Error>().use(...промис);

// Создаем внешний стор
const $myFormData = createStore<FormData>({
  title: 'example title',
  desc: 'example desc',
});

// Обновляем внешний стор при получении данных с бека
$myFormData.on(fxSendForm.doneData, (state, data) => data);

// Создаем форму и подюключаем внешний стор
const myForm = createForm<FormData>({
  initialValues: $myFormData,
  validationSchema: Zod.object({
    title: Zod.string().min(5),
    desc: Zod.string().max(100),
  }),
  onSubmit: fxSendForm,
});
```

### Использование в чистом виде

Форму можно использовать за рамками котролов и любых инпутов, достаточно напрямую вызывать команды из формы.

```tsx
//  По клику на этот div изменится значение в форме
<div onClick={() => myForm.setValue({path: 'title', value: 'test'})}></div>

//  По клику на эту картинку произойдет ввод формы
<img onClick={() => myForm.submit()}/>
```

### Отслеживание изменений

В данном примере мы подписываемся на триггер команды setValue.
Фильтруем по нужному имени контрола и получаем данные

```ts
sample({
  clock: testForm.setValue,
  filter: ({ path, value }) => path === 'books[1].title' && value === 'test text',
  fn: () => {
    console.log('Название первой книги изменено на "test text"');
  },
});
```

### Изменение значений на основе других

В данном примере мы подписываемся на триггер команды setValue.
Фильтруем по нужному имени контрола и получаем данные для дальнейшей установки значения в другой контрол

```ts
sample({
  clock: myForm.setValue,
  filter: ({ path, value }) => path === 'books[1].title',
  fn: ({ value }) => ({ path: 'books[1].desc', value: `${value} copied from title` }),
  target: myForm.setValue,
});
```
